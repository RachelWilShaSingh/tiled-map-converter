import json
import sys

def LoadTiledJsonMap( filename ):
    print( "LoadTiledJsonMap( " + filename + ")")
    mapFile = open( filename, "r" )
    mapDataText = mapFile.read()
    mapDataJson = json.loads( mapDataText )
    return mapDataJson



def ConvertMapFormat( jsonData ):
    print( "ConvertMapFormat(...)" )

    mapHeight = jsonData["height"]
    mapWidth = jsonData["width"]
    tileHeight = jsonData["tileheight"]
    tileWidth = jsonData["tilewidth"]

    mapData = {
        "metadata" : {
            "mapWidth" : mapWidth,
            "mapHeight" : mapHeight,
            "tileWidth" : tileWidth,
            "tileHeight" : tileHeight
        },
        "tiles" : []
        }

    z = 0

    # Iterate through all the layers of tile data...
    for layer in jsonData["layers"]:
        print( "Layer for z = " + str( z ) + ", named " + layer["name"] + "..." )
        # These may not be needed, but grabbing this data anyway.
        layerHeight = layer["height"]
        layerWidth = layer["width"]
        layerX = layer["x"]
        layerY = layer["y"]
        layerName = layer["name"]
        layerType = layer["type"]

        x = 0
        y = 0

        for data in layer["data"]:
            tileInfo = {
                "x" : x,
                "y" : y,
                "z" : z,
                "width" : tileWidth,
                "height" : tileHeight,
                "image" : data,
                "layer" : layerName
                }

            # Add this to the map data
            mapData["tiles"].append( tileInfo )

            # Keeping track of position of tiles
            x = x + 1
            if ( x >= layerWidth ):
                x = 0
                y = y + 1

        # End iterating through data

        z += 1
    # End iterating through layers

    return mapData


def OutputNewMap( filename, mapData ):
    print( "OutputNewMap( " + filename + " )" )
    mapFile = open( filename, "w" )

    # Output metadata
    mapFile.write( "MAP_DATA_BEGIN\n" )
    mapFile.write( "MAP_WIDTH   " + str( mapData["metadata"]["mapWidth"] ) + "\n" )
    mapFile.write( "MAP_HEIGHT  " + str( mapData["metadata"]["mapHeight"] ) + "\n" )
    mapFile.write( "TILE_WIDTH  " + str( mapData["metadata"]["tileWidth"] ) + "\n" )
    mapFile.write( "TILE_HEIGHT " + str( mapData["metadata"]["tileHeight"] ) + "\n" )
    mapFile.write( "MAP_DATA_END\n\n" )

    # Output tile data
    mapFile.write( "TILE_DATA_BEGIN\n\n" )

    for tile in mapData["tiles"]:
        if ( tile["image"] == 0 ):
            continue

        mapFile.write( "{: >10}".format( "TILE_BEGIN" ) )
        mapFile.write( "{: >10}".format( "X " + str( tile["x"] * tile["width"] ) ) )
        mapFile.write( "{: >10}".format( "Y " + str( tile["y"] * tile["height"] ) ) )
        mapFile.write( "{: >10}".format( "Z " + str( tile["z"] ) ) )
        mapFile.write( "{: >10}".format( "W " + str( tile["width"] ) ) )
        mapFile.write( "{: >10}".format( "H " + str( tile["height"] ) ) )
        mapFile.write( "{: >20}".format( "IMAGE " + str( tile["image"] ) ) )
        mapFile.write( "{: >20}".format( "LAYER " + str( tile["layer"] ) ) )
        mapFile.write( "{: >20}".format( "TILE_END\n" ) )

    mapFile.write( "\nTILE_DATA_END\n" )


mapDataJson = LoadTiledJsonMap( sys.argv[1] )
newMapData = ConvertMapFormat( mapDataJson )
OutputNewMap( sys.argv[1] + ".moo", newMapData )
